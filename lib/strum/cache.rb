# frozen_string_literal: true

require "dry/inflector"
require "dry/configurable"
require "strum/cache/version"
require "strum/service"
require "strum/pipe"
require "strum/json"
require "strum/cache_utils/redis"
require "strum/cache_utils/find"
require "strum/cache_utils/search"

module Strum
  # rubocop: disable Style/Documentation
  module Cache
    extend Dry::Configurable

    setting :cache_headers
    setting :redis_class, Redis

    class Error < StandardError; end

    # rubocop: disable Metrics/AbcSize,Metrics/MethodLength
    def self.const_missing(resource_name)
      # Strum::Cache::Entity::Find.(id)
      # Strum::Cache::Entity::Search.(params)
      # Strum::Cache::Entity::Put.(params)

      Module.new do
        const_set(:Push, Class.new do
          include Strum::Service

          define_method :call do
            redis_connection.hset(id, input)
            redis_connection.expire(id, redis_resource_expire)
          end

          define_method :audit do
            required(:id)
          end

          private

            define_method :resource_code do
              resource_name.downcase.to_s
            end

            define_method :redis_connection do
              Strum::CacheUtils::RedisStorage.const_get(resource_code.capitalize, false).instance.redis
            end

            def redis_resource_expire
              ENV.fetch("#{resource_code.to_s.upcase}_CACHE_EXPIRE", ENV.fetch("CACHE_EXPIRE", 60 * 15))
            end
        end)

        const_set(:Find, Class.new do
          include Strum::Service

          define_method :call do
            if (entity = from_cache_by_id(input) || find_by_id(input))
              output(entity)
            else
              not_found
            end
          end

          define_method :audit do
            self.input = input.to_s.to_i
            add_error(:id, :invalid) unless input.positive?
          end

          private

            define_method :resource_code do
              resource_name.downcase
            end

            define_method :from_cache_by_id do |resource_id|
              Strum::CacheUtils::Redis.call(resource_code: resource_code, resource_id: resource_id)
            end

            define_method :find_by_id do |resource_id|
              Strum::CacheUtils::Find.call(resource_code: resource_code, resource_id: resource_id) do |m|
                m.success { |result| result }
                m.failure do |errors|
                  add_errors(errors)
                  nil
                end
              end
            end

            define_method :not_found do
              add_error(:entity, :not_found)
            end
        end)

        const_set(:Search, Class.new do
          include Strum::Service

          define_method :call do
            if (entity = from_cache_by_params || search_by_params)
              output(entity)
            else
              not_found
            end
          end

          define_method :audit do
            required
          end

          private

            define_method :resource_code do
              resource_name.downcase
            end

            define_method :from_cache_by_params do
              false
            end

            define_method :search_by_params do
              Strum::CacheUtils::Search.call(resource_code: resource_code, params: input) do |m|
                m.success { |result| result }
                m.failure do |errors|
                  add_errors(errors)
                  nil
                end
              end
            end

            define_method :not_found do
              add_error(:entity, :not_found)
            end
        end)
      end
    end
    # rubocop: enable Metrics/AbcSize,Metrics/MethodLength
  end
  # rubocop: enable Style/Documentation
end
