# frozen_string_literal: true

require "strum/cache_utils/shared/mixins/url"
require "strum/cache_utils/build_resources_url"

module Strum
  module CacheUtils
    # Resource url builder
    class BuildResourceUrl
      include Strum::Service
      include Shared::Mixins::Url

      private

        RESOURCE_SUFIX_URL = "RESOURCE_URL"
        HOST = "BASE_RESOURCE_URL"

        def audit
          required(:resource_code, :resource_id)
          sliced(:resource_code, :resource_id)
        end

        def call
          output(url: File.join(base_url, resource_id.to_s))
        rescue KeyError
          Strum::CacheUtils::BuildResourcesUrl.call(resource_code: resource_code, params: { id: resource_id }) do |m|
            m.success { |result| output(result) }
            m.failure { |errors| add_errors(errors) }
          end
        end
    end
  end
end
