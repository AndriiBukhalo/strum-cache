# frozen_string_literal: true

require "strum/cache_utils/build_resources_url"
require "strum/cache_utils/send_request"

module Strum
  module CacheUtils
    # Search entity
    class Search
      include Strum::Service

      def audit
        required(:resource_code, :params)
        sliced(:resource_code, :params)
      end

      def call
        Strum::Pipe.call(Strum::CacheUtils::BuildResourcesUrl,
                         Strum::CacheUtils::SendRequest,
                         Strum::Json::Deserializer,
                         input: input) do |m|
          m.success { |responce| output(responce) }
          m.failure { |errors| add_errors(errors) }
        end
      end
    end
  end
end
