# frozen_string_literal: true

require "strum/cache_utils/build_resource_url"

module Strum
  module CacheUtils
    # Find service
    class Find
      include Strum::Service

      def audit
        required(:resource_code, :resource_id)
        sliced(:resource_code, :resource_id)
      end

      def call
        Strum::Pipe.call(Strum::CacheUtils::BuildResourceUrl,
                         Strum::CacheUtils::SendRequest,
                         Strum::Json::Deserializer,
                         input: input) do |m|
          m.success { |responce| output(responce) }
          m.failure { |errors| add_errors(errors) }
        end
      end
    end
  end
end
