# frozen_string_literal: true

require "uri"
require "strum/cache_utils/shared/mixins/url"

module Strum
  module CacheUtils
    # Build url to resource
    class BuildResourcesUrl
      include Strum::Service
      include Shared::Mixins::Url

      private

        RESOURCE_SUFIX_URL = "RESOURCE_SEARCH_URL"
        HOST = "BASE_RESOURCE_SEARCH_URL"

        def audit
          required(:resource_code, :params)
          sliced(:resource_code, :params)

          add_error(:params, :hash_required) unless params.is_a?(Hash)
        end

        def call
          output(url: base_url, params: params)
        rescue KeyError
          add_error("#{resource_code} SEARCH ENV", :not_found)
        end
    end
  end
end
