# Changelog
All notable changes to this project will be documented in this file.

## [0.1.3] - 2021-08-09
### Changed
- adjust error when failure. Add some info data

## [0.1.1] - 2021-07-29
### Changed
- adjust to strum/json
