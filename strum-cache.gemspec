# frozen_string_literal: true

require_relative "lib/strum/cache/version"

Gem::Specification.new do |spec|
  spec.name          = "strum-cache"
  spec.version       = Strum::Cache::VERSION
  spec.authors       = ["Serhiy Nazarov"]
  spec.email         = ["sn@nazarov.com.ua"]

  spec.summary       = "Cache resources"
  spec.homepage      = "https://gitlab.com/strum-rb/strum-cache"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/strum-rb/strum-cache"
  spec.metadata["changelog_uri"] = "https://gitlab.com/strum-rb/strum-cache/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(
        /^(test|spec|features|\.rubocop.yml|\.ruby-version|Gemfile.lock|Gemfile|\.gitlab-ci.yml|README|\.travis.yml|CHANGELOG|\.rspec)/ # rubocop:disable Layout/LineLength
      )
    end
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "dry-configurable", "~> 0.12.1"
  spec.add_runtime_dependency "dry-inflector", "~> 0.2"
  spec.add_runtime_dependency "faraday", "~> 1.4"
  spec.add_runtime_dependency "faraday_middleware", "~> 1.0"
  spec.add_runtime_dependency "json", "~> 2.3"
  spec.add_runtime_dependency "redis", "~> 4.2"
  spec.add_runtime_dependency "strum-json", "~> 0.0.3"
  spec.add_runtime_dependency "strum-pipe", "~> 0.0.3"
  spec.add_runtime_dependency "strum-service", "~> 0.1"
end
